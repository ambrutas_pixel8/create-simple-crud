<?php
require_once "connect.php";

if (isset($_GET['delid'])) {
    $eid = $_GET['delid'];
    $sql = mysqli_query($conn, "DELETE FROM employee WHERE id='$eid'");
    if ($sql) {
        echo "<script> alert('Record Deleted Successfully');</script>";
        echo "<script>document.location='read.php';</script>";
    } else {
        echo "<script> alert('Failed to delete record.');</script>";
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Simple CRUD | READ</title>

    <style>
        table {
            width: 100%;
            border-collapse: collapse;
        }

        th, td {
            padding: 8px;
            text-align: left;
            border-bottom: 1px solid #ddd;
        }

        th {
            background-color: #f2f2f2;
        }
    </style>
</head>
<body>
<h1>Employee Information</h1>
    <table>
        <thead>
            <th>No.</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Middle Name</th>
            <th>Birthday</th>
            <th>Address</th>
            <th>Action</th>
            <th></th>
        </thead>
        <tbody>
            <?php
            require_once "connect.php";
            $sql = mysqli_query($conn, "SELECT * FROM employee;");
            $count = 1;
            $row = mysqli_num_rows($sql);

            if ($row > 0) {
                while ($row = mysqli_fetch_array($sql)) {
            ?>
            <tr>
                <td><?php echo $count; ?></td>
                <td><?php echo $row['first_name']; ?></td>
                <td><?php echo $row['last_name']; ?></td>
                <td><?php echo $row['middle_name']; ?></td>
                <td><?php echo $row['birthday']; ?></td>
                <td><?php echo $row['address']; ?></td>
                <td>
                    <a href="update.php?editid=<?php echo htmlentities($row['id']);?>">
                        <button>Edit</button>
                    </a>
                </td>
                <td>
                    <a href="read.php?delid=<?php echo htmlentities($row['id']);?>" onclick="return confirm('Do you really want to delete this record?');">
                        <button>Delete</button>
                    </a>
                </td>
            </tr>
            <?php
                $count = $count + 1;
                }
            }
            ?>
        </tbody>
    </table>

    <div style="margin-top:1%">
        <a href="create.php"> 
            <button> Add new record </button>
        </a>
    </div>
</body>
</html>
