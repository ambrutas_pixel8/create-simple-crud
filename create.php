<?php
    require_once "connect.php";

    if(isset($_POST['submit'])){
        $fname = $_POST['firstname'];
        $lname = $_POST['lastname'];
        $mname = $_POST['middlename'];
        $birthday = $_POST['birthday'];
        $address = $_POST['address'];

        $sql = mysqli_query($conn, "INSERT INTO employee (first_name, last_name, middle_name, birthday, address)
                                    VALUES ('$fname', '$lname', '$mname', '$birthday', '$address');"); 
        
        if($sql){
            echo "<script>alert('New Record Successfully Added');</script>";
            echo "<script>document.location='create.php';</script>";
        }else{
            echo "<script>alert('Something Went Wrong');</script>";
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Simple CRUD | CREATE</title>
    <style>
        .form-container {
            width: 300px;
            margin: 0 auto;
            text-align: center;
        }
    </style>
</head>
<body>
    <div class="form-container">
        <h1>Employee Registration Form</h1>
        <form method="POST">
            <div>
                <label>First Name:</label><br>
                <input type="text" name="firstname" class="form-control" placeholder="Enter First Name" required>
            </div>

            <div>
                <label>Last Name:</label><br>
                <input type="text" name="lastname" class="form-control" placeholder="Enter Last Name" required>
            </div>

            <div>
                <label>Middle Name:</label><br>
                <input type="text" name="middlename" class="form-control" placeholder="Enter Middle Name" required>
            </div>

            <div>
                <label>Birthdate:</label><br>
                <input type="date" name="birthday" class="form-control">
            </div>

            <div>
                <label>Address:</label><br>
                <input type="text" name="address" class="form-control" placeholder="Enter Address" required>
            </div>

            <div style="margin-top: 2%;">
                <button type="submit" name="submit">Submit</button> <br> <br>
                <a href="read.php" class="btn btn-success">View Record</a>
            </div>
        </form>
    </div>
</body>
</html>
