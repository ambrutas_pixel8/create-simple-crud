<?php

require_once "connect.php";
    if(isset($_POST['edit'])){
        $eid=$_GET['editid'];
        $fname = $_POST['firstname'];
        $lname = $_POST['lastname'];
        $mname = $_POST['middlename'];
        $birthday = $_POST['birthday'];
        $address = $_POST['address'];

        $sql = mysqli_query($conn, "UPDATE employee SET first_name= '$fname', last_name = '$lname', middle_name= '$mname', birthday ='$birthday' , address= '$address' WHERE id='$eid'");
        if($sql){
            echo "<script> alert('Record Updated Successfully');</script>";
            echo "<script>document.location='read.php';</script>";
        }else {
            echo "<script> alert('Something went wrong');</script>";
        }
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Simple CRUD | CREATE</title>
    <style>
        .form-container {
            width: 300px;
            margin: 0 auto;
            text-align: center;
        }
    </style>
</head>
<body>
    <div class="form-container">
        <h1>Update Record</h1>
        <form method="POST">
            <?php
                $eid =$_GET['editid'];
                $sql = mysqli_query($conn, "SELECT * FROM employee WHERE id='$eid'");
                    while($row=mysqli_fetch_array($sql)){
                ?>            
            <div>
                <label>First Name:</label><br>
                <input type="text" name="firstname" value="<?php echo $row['first_name']?>" class="form-control" placeholder="Enter First Name" required>
            </div>

            <div>
                <label>Last Name:</label><br>
                <input type="text" name="lastname" value="<?php echo $row['last_name']?>" class="form-control" placeholder="Enter Last Name" required>
            </div>

            <div>
                <label>Middle Name:</label><br>
                <input type="text" name="middlename" value="<?php echo $row['middle_name']?>" class="form-control" placeholder="Enter Middle Name" required>
            </div>

            <div>
                <label>Birthdate:</label><br>
                <input type="date"value="<?php echo $row['birthday']?>" name="birthday" class="form-control">
            </div>

            <div>
                <label>Address:</label><br>
                <input type="text" name="address" value="<?php echo $row['address']?>" class="form-control" placeholder="Enter Address" required>
            </div>
        <?php
        }
        ?>
            <div style="margin-top: 2%;">
                <button type="submit" name="edit">Update</button> <br> <br>
                <a href="read.php" class="btn btn-success">View Record</a>
            </div>
        </form>
    </div>
</body>
</html>
